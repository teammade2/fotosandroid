package com.example.ivans.made2fotos;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    /*Arraylist para almacenar los datos de la bd de Firebase*/
    ArrayList<datos> listDatos;

    RecyclerView recyclerView;
    ProgressDialog progres ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /* Levanto un progress hasta obtener todas las url para comenzar a descargar.
         Una vez obtenida la lista de url, la descarga de las imagenes es asincronica.
        */
        progres = new ProgressDialog(this);
        progres.setMessage("Espere por favor, recibiendo datos...");
        progres.setCancelable(false);
        progres.show();
        listDatos = new ArrayList();

        /*Se utiliza el fab para lanzar la activity TakePicture*/
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent( view.getContext(), TakePicture.class);
                startActivity(i);
            }
        });

        /*Se implementa un recliclerView para mostrar la lista de datos*/
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 1);
        recyclerView = (RecyclerView) findViewById(R.id.rv_images);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);

        /*se obtiene todos las url de las imagenes a descargar y los titulos de los mismos*/
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                listDatos.clear();
                for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                    listDatos.add(new datos(snapshot.getKey(),snapshot.getValue().toString()));
                }
                /*Una vez obtenida dicha lista finalizo el progres dialogo y le paso al adapter la lista de datos
                a mostrar, se "infla" el recilcerView con el adapter.*/
                progres.dismiss();
                myAdapter adapter = new myAdapter(listDatos);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        /*obtengo una referencia de la bd sobre la raiz y asocio el listener*/
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        database.getReference("").addValueEventListener(postListener);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}