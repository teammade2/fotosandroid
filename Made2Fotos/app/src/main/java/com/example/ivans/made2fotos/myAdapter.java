package com.example.ivans.made2fotos;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.data.DataFetcher;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.stream.StreamModelLoader;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StreamDownloadTask;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;


/**
 * Created by ivans on 25/05/17.
 * Implemento un adapter para  "inflar" el reciclerView
 * Se utiliza Glide para descargar y guardar en memoria cache las imágenes a mostrar
 */



public class myAdapter extends RecyclerView.Adapter<myAdapter.MyViewHolder>  {

    private ArrayList<datos> listDatos;
    Context context;
    @Override
    public myAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View photoView = inflater.inflate(R.layout.item, parent, false);
        myAdapter.MyViewHolder viewHolder = new myAdapter.MyViewHolder(photoView);

        return viewHolder;
    }
    public myAdapter(ArrayList<datos> listDatos) {
        this.listDatos= listDatos;
    }

    @Override
    public void onBindViewHolder(myAdapter.MyViewHolder holder, int position) {

        ImageView imageView = holder.mPhotoImageView;
        TextView tvItemTitle =  holder.tvItemTitle;

        String url = this.listDatos.get(position).getmUrl();
        String title = this.listDatos.get(position).getmTitle();
        tvItemTitle.setText(title);

        /*De cada url de la listDatos pasada por parametro al adapter
        se realiza la descarga de la imagen que fue almacenada en el Store de Firebase*/
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReferenceFromUrl("gs://vehiculotracker.appspot.com");
        StorageReference imagesRef = storageRef.child(url);
        Glide.with(context)
                .using(new FirebaseImageLoader())
                .load(imagesRef)
                .placeholder(R.drawable.common_full_open_on_phone)
                .diskCacheStrategy(DiskCacheStrategy.ALL) //use this to cache
                .centerCrop()
                .crossFade()
                .into(imageView);
    }

    @Override
    public int getItemCount() {
        return (this.listDatos.size());
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public CardView cv;
        public ImageView mPhotoImageView;
        public TextView tvItemTitle;
        public MyViewHolder(View itemView) {

            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cv);
            mPhotoImageView = (ImageView) itemView.findViewById(R.id.iv_photo);
            tvItemTitle = (TextView)  itemView.findViewById(R.id.tvItemTitle);
            itemView.setOnClickListener(this);
        }

        /* Implemento el onClick de cada imagen, se lanza una activity para mostrar en fulscreen la imagen*/
        @Override
        public void onClick(View view) {

            int position = getAdapterPosition();
            if(position != RecyclerView.NO_POSITION) {
                datos d = listDatos.get(position);
                Intent intent = new Intent(context, Detalle.class);
                intent.putExtra("url", d.getmUrl());

                context.startActivity(intent);
            }
        }
    }


    /**
     * ModelLoader implementation to download images from FirebaseStorage with Glide.
     *
     * Sample Usage:
     * <pre>
     *     StorageReference ref = FirebaseStorage.getInstance().getReference().child("myimage");
     *     ImageView iv = (ImageView) findViewById(R.id.my_image_view);
     *
     *     Glide.with(this)
     *         .using(new FirebaseImageLoader())
     *         .load(ref)
     *         .into(iv);
     * </pre>
     */
 public static class FirebaseImageLoader implements StreamModelLoader<StorageReference> {

     private static final String TAG = "FirebaseImageLoader";

     @Override
     public DataFetcher<InputStream> getResourceFetcher(StorageReference model, int width, int height) {
         return new FirebaseStorageFetcher(model);
     }

     private class FirebaseStorageFetcher implements DataFetcher<InputStream> {

         private StorageReference mRef;
         private StreamDownloadTask mStreamTask;
         private InputStream mInputStream;

         FirebaseStorageFetcher(StorageReference ref) {
             mRef = ref;
         }

         @Override
         public InputStream loadData(Priority priority) throws Exception {
             mStreamTask = mRef.getStream();
             mInputStream = Tasks.await(mStreamTask).getStream();

             return mInputStream;
         }

         @Override
         public void cleanup() {
             // Close stream if possible
             if (mInputStream != null) {
                 try {
                     mInputStream.close();
                     mInputStream = null;
                 } catch (IOException e) {
                     Log.w(TAG, "Could not close stream", e);
                 }
             }
         }

         @Override
         public String getId() {
             return mRef.getPath();
         }

         @Override
         public void cancel() {
             // Cancel task if possible
             if (mStreamTask != null && !mStreamTask.isComplete()) {
                 mStreamTask.cancel();
             }
         }
     }
 }
}
