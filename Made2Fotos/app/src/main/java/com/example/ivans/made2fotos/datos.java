package com.example.ivans.made2fotos;

/**
 * Created by ivans on 25/05/17.
 */

public class datos {

    private String mUrl;
    private String mTitle;

    public datos(String mUrl, String mTitle) {
        this.mUrl = mUrl;
        this.mTitle = mTitle;
    }

    public String getmUrl() {
        return mUrl;
    }

    public void setmUrl(String mUrl) {
        this.mUrl = mUrl;
    }

    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }
}


